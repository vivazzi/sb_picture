=======
sb_picture
=======

sb_picture is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds image to page.


Installation
============

sb_picture requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_picture in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_picture


Configuration 
=============

1. Add "sb_picture" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_picture',
        ...
    )

2. Run `python manage.py migrate` to create the sb_picture models.  


Licensing
=========

sb_picture is free software under terms of the MIT License.


MIT License

Copyright (c) Artem Maltsev, artem@vits.pro, maltsev.artjom@gmail.com, vivazzi.pro
Company: Vuspace, vuspace.pro
Web studio: Viva IT Studio, vits.pro

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
