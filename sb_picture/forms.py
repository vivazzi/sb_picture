from django import forms

from sb_core import settings
from sb_picture.models import SBPicture


class SBPictureForm(forms.ModelForm):
    class Meta:
        model = SBPicture
        exclude = ()

    class Media:
        js = [settings.JQUERY_URL, 'sb_core/js/sb_library.js', 'sb_picture/sb_picture_admin.js', ]
