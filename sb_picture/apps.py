from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBPictureConfig(AppConfig):
    name = 'sb_picture'
    verbose_name = _('Picture')
