from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from sb_core.constants import BASE
from sb_picture.forms import SBPictureForm
from sb_picture.models import SBPicture


class SBPicturePlugin(CMSPluginBase):
    module = BASE
    model = SBPicture
    name = 'Картинка'
    form = SBPictureForm
    render_template = 'sb_picture/sb_picture.html'
    text_enabled = True

    fieldsets = (
        ('', {
            'fields': ('pic', 'hint',
                       ('alignment', 'float', 'mapping'),
                       ('width', 'height'),
                       'is_full_display',
                       # ('tag', 'use_lazy'),
                       'tag',
                       )
        }),
        ('Внешний вид картинки', {
            'fields': (('is_shadow', 'shadow_pars'),
                       ('is_border', 'border_size', 'border_color'),
                       'no_margin')
        }),
        ('Группа', {
            'fields': ('group', )
        }),
    )

    def render(self, context, instance, placeholder):
        context = super(SBPicturePlugin, self).render(context, instance, placeholder)

        context['is_full_display'] = instance.is_full_display

        parent = instance.parent
        if parent and parent.get_plugin_instance() and hasattr(parent.get_plugin_instance()[0], 'plugin_type') and \
                        parent.get_plugin_instance()[0].plugin_type == 'SBLinkPlugin':
            context['is_full_display'] = False
        return context

plugin_pool.register_plugin(SBPicturePlugin)
