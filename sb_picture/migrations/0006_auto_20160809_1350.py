from django.db import models, migrations
import sb_core.fields


def set_blank(apps, schema_editor):
    SBPicture = apps.get_model('sb_picture', 'SBPicture')
    for obj in SBPicture.objects.all():
        obj.attr_class = obj.attr_class or ''
        obj.border_color = obj.border_color or ''
        obj.folder = obj.folder or ''
        obj.hint = obj.hint or ''
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_picture', '0005_auto_20160603_2314'),
    ]

    operations = [
        migrations.RunPython(set_blank),
        migrations.AlterField(
            model_name='sbpicture',
            name='attr_class',
            field=models.CharField(default='', help_text='\u0412\u0441\u043f\u043e\u043c\u043e\u0433\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043a\u043b\u0430\u0441\u0441 \u0434\u043b\u044f \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u0430', max_length=50, verbose_name='\u041a\u043b\u0430\u0441\u0441', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbpicture',
            name='border_color',
            field=sb_core.fields.ColorField(default='#fff', help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430<br/><a rel="nofollow" target="_blank" href="http://getcolor.ru/">\u0421\u0435\u0440\u0432\u0438\u0441 \u043f\u043e \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044e \u0446\u0432\u0435\u0442\u0430</a>', max_length=30, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446', blank=True),
        ),
        migrations.AlterField(
            model_name='sbpicture',
            name='folder',
            field=models.CharField(default='', max_length=5, editable=False, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbpicture',
            name='hint',
            field=models.CharField(default='', help_text='\u041f\u043e\u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0438 \u043d\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0438 \u043c\u044b\u0448\u043a\u043e\u0439 \u043d\u0430 \u043e\u0431\u044a\u0435\u043a\u0442', max_length=255, verbose_name='\u041f\u043e\u0434\u0441\u043a\u0430\u0437\u043a\u0430', blank=True),
            preserve_default=False,
        ),
    ]
