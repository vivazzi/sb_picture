from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_picture', '0006_auto_20160809_1350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbpicture',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_picture_sbpicture', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
