from django.db import models, migrations
import django.core.validators
import sb_core.cms_models
import sb_core.folder_mechanism


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBPicture',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('folder', models.CharField(max_length=5, null=True, editable=False, blank=True)),
                ('pic', models.ImageField(upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=255, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430')),
                ('hint', models.CharField(help_text='\u041f\u043e\u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0438 \u043d\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0438 \u043c\u044b\u0448\u043a\u043e\u0439 \u043d\u0430 \u043e\u0431\u044a\u0435\u043a\u0442', max_length=255, null=True, verbose_name='\u041f\u043e\u0434\u0441\u043a\u0430\u0437\u043a\u0430', blank=True)),
                ('is_full_display', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0432\u0435\u0441\u044c \u044d\u043a\u0440\u0430\u043d \u043f\u0440\u0438 \u0449\u0435\u043b\u0447\u043a\u0435?')),
                ('float', models.CharField(blank=True, max_length=10, null=True, verbose_name='\u041e\u0431\u0442\u0435\u043a\u0430\u043d\u0438\u0435', choices=[('left', '\u043b\u0435\u0432\u043e\u0435'), ('right', '\u043f\u0440\u0430\u0432\u043e\u0435')])),
                ('alignment', models.CharField(blank=True, max_length=10, null=True, verbose_name='\u0412\u044b\u0440\u0430\u0432\u043d\u0438\u0432\u0430\u043d\u0438\u0435', choices=[('left', '\u0432\u043b\u0435\u0432\u043e'), ('center', '\u043f\u043e \u0446\u0435\u043d\u0442\u0440\u0443'), ('right', '\u0432\u043f\u0440\u0430\u0432\u043e')])),
                ('width', models.PositiveIntegerField(null=True, verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430, \u043f\u0438\u043a\u0441', blank=True)),
                ('height', models.PositiveIntegerField(null=True, verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430, \u043f\u0438\u043a\u0441', blank=True, help_text='\u0427\u0430\u0449\u0435 \u0432\u0441\u0435\u0433\u043e \u0432\u044b\u0441\u043e\u0442\u0443 \u043d\u0435 \u043d\u0443\u0436\u043d\u043e \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0434\u043b\u044f \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u044f \u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0439 \u0438\u0437\u043e\u0431\u0440\u0430\u0449\u0435\u043d\u0438\u044f')),
                ('is_shadow', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0442\u0435\u043d\u044c?')),
                ('shadow_pars', models.CharField(default='0 0 10px 0 rgba(0,0,0,0.5)', max_length=100, blank=True, help_text='<\u0441\u0434\u0432\u0438\u0433 \u043f\u043e x> <\u0441\u0434\u0432\u0438\u0433 \u043f\u043e y> <\u0440\u0430\u0434\u0438\u0443\u0441 \u0440\u0430\u0437\u043c\u044b\u0442\u0438\u044f> <\u0440\u0430\u0441\u0442\u044f\u0436\u0435\u043d\u0438\u0435> <\u0446\u0432\u0435\u0442></br>\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: 0 0 10px 0 rgba(0, 0, 0, 0.5), 2px 2px 7px 1px rgba(0, 0, 0, 0.4)', null=True, verbose_name='\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u0442\u0435\u043d\u0438')),
                ('is_border', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0433\u0440\u0430\u043d\u0438\u0446\u0443?')),
                ('border_size', models.PositiveSmallIntegerField(default=3, null=True, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u0433\u0440\u0430\u043d\u0438\u0446, \u043f\u0438\u043a\u0441', blank=True, validators=[django.core.validators.MinValueValidator(1)])),
                ('border_color', models.CharField(default='#fff', max_length=255, blank=True, help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', null=True, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446')),
                ('attr_class', models.CharField(help_text='\u0412\u0441\u043f\u043e\u043c\u043e\u0433\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043a\u043b\u0430\u0441\u0441 \u0434\u043b\u044f \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u0430', max_length=50, null=True, verbose_name='\u041a\u043b\u0430\u0441\u0441', blank=True)),
            ],
            options={
                'db_table': 'sb_picture',
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438',
            },
            bases=(sb_core.cms_models.SBCMSPluginMixin, 'cms.cmsplugin', models.Model),
        ),
    ]
