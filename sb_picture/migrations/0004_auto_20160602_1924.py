from django.db import models, migrations
import sb_core.folder_mechanism
import sb_core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sb_picture', '0003_auto_20160601_1833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbpicture',
            name='pic',
            field=sb_core.fields.PicField(upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=255, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430'),
        ),
    ]
