from os.path import basename

from django.core.validators import MinValueValidator
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe

from sb_core.constants import color_ht, attr_class_ht, hint_ht, shadow_ht
from sb_core.fields import PicField, ColorField
from sb_core.cms_models import PicModelPlugin
from sb_core.templatetags.sb_thumb_tags import do_retina


class SBPicture(PicModelPlugin):
    LEFT = 'left'
    CENTER = 'center'
    RIGHT = 'right'
    ALIGNMENT_CHOICES = ((LEFT, 'влево'),
                         (CENTER, 'по центру'),
                         (RIGHT, 'вправо'))
    FLOAT_CHOICES = ((LEFT, 'левое'),
                     (RIGHT, 'правое'))

    pic = PicField('Картинка')

    hint = models.CharField('Подсказка', max_length=255, blank=True, help_text=hint_ht)

    is_full_display = models.BooleanField('Открывать на весь экран при щелчке?', default=True)

    # alignments
    alignment = models.CharField('Выравнивание', max_length=10, blank=True, null=True, choices=ALIGNMENT_CHOICES)
    float = models.CharField('Обтекание', max_length=10, blank=True, null=True, choices=FLOAT_CHOICES)

    COVER = 'cover'
    CONTAIN = 'contain'
    MAPPING_CHOICES = ((COVER, 'Покрыть'), (CONTAIN, 'Вместить'))
    mapping = models.CharField(
        'Отображение', max_length=7, choices=MAPPING_CHOICES, default=COVER,
        help_text='Если выбрано "Покрыть" и заданы размеры, то не вошедшие в размеры края будут обрезаны '
                  '(больше подходит для фотографий).<br/>'
                  'Для иконок подойдёт "Вместить" для того, чтобы края иконок не обрезались')

    # sizes
    change_message_help_text = 'Если указать значение, то кроме обрезки изображения также будет и сжатие изображения'

    width = models.PositiveIntegerField(
        'Ширина, пикс', blank=True, null=True,
        help_text=change_message_help_text
    )
    height = models.PositiveIntegerField(
        'Высота, пикс', blank=True, null=True,
        help_text=f'{change_message_help_text}<br/>Чаще всего высоту не нужно указывать для сохранения пропорций изображения'
    )

    # styles
    is_shadow = models.BooleanField('Добавить тень?', default=False)
    shadow_pars = models.CharField('Параметры тени', max_length=100, blank=True, null=True,
                                   default='0 0 10px 0 rgba(0,0,0,0.5)', help_text=shadow_ht)

    is_border = models.BooleanField('Добавить границу?', default=False)
    border_size = models.PositiveSmallIntegerField('Размер границ, пикс', default=3, null=True, blank=True,
                                                   validators=[MinValueValidator(1), ])
    border_color = ColorField('Цвет границ', blank=True, default='#fff', help_text=color_ht)

    IMG = 'img'
    DIV = 'div'
    TAGS = ((IMG, 'img'), (DIV, 'div'))
    tag = models.CharField('Тег', max_length=3, choices=TAGS, default=IMG,
                           help_text='Для картинок лучше использовать тег img, а для иконок - div')

    # deprecation. We can use NullBooleanField for use_lazy with values: lazy, eager, auto.
    use_lazy = models.BooleanField('Использовать отложенную загрузку изображения?', default=True,
                                   help_text='Изображение будут загружаться только тогда, когда пользователь '
                                             'перемотает страницу до изображения.<br/>Данная техника увеличивает '
                                             'производительность, но для некоторых случаев её можно отключить.')
    # todo: remove attr_class from plugins
    attr_class = models.CharField('Класс', max_length=50, blank=True, help_text=attr_class_ht)

    no_margin = models.BooleanField('Убрать верхние и нижние отступы?', default=False,
                                    help_text='Полезно для некоторых случаев')

    group = models.CharField(
        'Группа', max_length=100, blank=True,
        help_text='Для объединения всплывающих окон задайте для каждого всплывающего окна одинаковое название группы'
    )

    @property
    def style(self):
        styles = ''
        styles += 'border: {}px solid {};'.format(self.border_size, self.border_color) if self.is_border else ''
        styles += 'box-shadow: {};'.format(self.shadow_pars) if self.is_shadow else ''
        return styles

    def html(self, request):
        bg_css = None if self.has_size_2x() else "background-image: url('{}')"

        if self.width or self.height:
            is_retina = self.has_size_2x()
            if is_retina is True:
                is_retina = None

            return do_retina(self, self.get_width(), self.get_height(), self.hint, self.mapping, tag=self.tag,
                             is_retina=is_retina, style=self.style, th_class='th', bg_css=bg_css)

        if self.tag == self.DIV:
            css = f'background-image: url(\'{self.pic.url}\');background-size: contain;' \
                  f'width: 100%;height: 100%;aspect-ratio: {self.pic.width / self.pic.height};'
            return mark_safe(f'<div style="{css}"></div>')

        return mark_safe(f'<img src="{self.pic.url}" loading="lazy" />')

    def get_height(self):
        if self.height:
            return self.height

        if self.pic.width != 0:
            return int(self.get_width() * self.pic.height / self.pic.width)

        return 0

    def get_padding_top(self):
        value = self.get_height() / self.get_width()
        return '{}%'.format(round(value, 2) * 100)

    def has_size_2x(self):
        has_size = bool(self.width or self.height)
        if not has_size:
            return False

        if self.width and self.width * 2 > (self.pic.width or 0):
            return False

        # todo: now checking of height get bug, because we can use any height and use retina
        # if self.height and self.height * 2 < self.pic.height:
        #     return False

        return True

    def get_width(self):
        return self.width or self.pic.width

    def __str__(self):
        return self.hint or basename(self.pic.name)

    def clean(self):
        if self.float and self.alignment:
            raise ValidationError('Пожалуйста, выберите либо выравнивание картинки, либо обтекание картинки')

        if self.is_shadow and not self.shadow_pars:
            raise ValidationError('При использовании тени укажите значение в поле "Параметры тени"')

        if self.is_border and not (self.border_size and self.border_color):
            raise ValidationError(
                'При использовании границы укажите значения в обоих полях: "Размер границ", "Цвет границ"'
            )

    class Meta:
        db_table = 'sb_picture'
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
